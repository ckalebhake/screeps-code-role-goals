import { ErrorMapper } from "utils/ErrorMapper";
import { createRequiredMemoryObjects } from "./utils/lib.defaults";
import { getRoleByName } from "./roles/GetRoleByName";
import { roomLog } from "./utils/lib.log";
import { spawnCreeps } from "./spawnCreeps";
import { assignGoals, assignRoleGoals } from "./assignGoals";
import { runBehaviorByName } from "./behaviors/runBehaviorByName";
import { createBasicRoomRolesIfAbsent } from "./roles/createBasicRoomRolesIfAbsent";
import { BehaviorIdle } from "./behaviors/MiscBehaviors";
import { GeneralRoomGoalProvider } from "./GeneralRoomGoalProvider";
import { logRoomStatistics, roomAverages, updateRoomStatistics } from "./utils/lib.statistics";

// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
//TODO; allow creep requests to be fulfilled by assigning an existing, non-specialized creep to them instead of spawning a new unit
export const loop = ErrorMapper.wrapLoop(() => {
    // console.log("==========[" + Game.time + "]==========");

    //ensure required memory objects exist for each room
    createRequiredMemoryObjects();

    //ensure basic roles exist for all rooms
    _.forEach(Game.rooms, room => createBasicRoomRolesIfAbsent(room));

    //gather up all of the creepRequests and roles on the map
    const creepRequests: CreepRequest[] = [];
    const rolesAndStates: { role: Role, state: RoleState }[] = [];

    //roles targeting entire rooms
    Memory.roles.roomRoles.forEach(roleState => {
        const role = getRoleByName(roleState.roleName) as RoleForRoom;
        if (!role) {
            roomLog(roleState.target, LogType.error,
                "room has a role " + roleState.roleName + ", but could not find a role by that name!");
            return;
        }

        creepRequests.push(...role.getCreepRequests(roleState));
        rolesAndStates.push({ role: role, state: roleState });
    });

    //roles targeting objects inside of rooms
    Memory.roles.roomObjectRoles.forEach(roleState => {
        const role = getRoleByName(roleState.roleName) as RoleForRoomObject;
        if (!role) {
            roomLog(roleState.target.roomName, LogType.error,
                "room has a role " + roleState.roleName + ", but could not find a role by that name!");
            return;
        }

        creepRequests.push(...role.getCreepRequests(roleState));
        rolesAndStates.push({ role: role, state: roleState });
    });

    //perform spawning
    spawnCreeps(creepRequests);

    //goal assignment for roles
    rolesAndStates.forEach(i => { assignRoleGoals(i.role, i.state) });

    //general goal assignment for each room
    _.forEach(Game.rooms, room => {
        assignGoals(room.find(FIND_MY_CREEPS, {
            filter: c => {
                return !c.memory.specialized && !c.memory.goal;
            }
        }), GeneralRoomGoalProvider.getGoals(room));
    });

    //room summaries
    _.forEach(Game.rooms, room => {
        if (room.controller && room.controller.my) {
            const roomCreeps = room.find(FIND_MY_CREEPS);
            roomLog(room.name, LogType.summary, "creeps: " + roomCreeps.length + " (" +
                JSON.stringify(_.countBy(roomCreeps, rc => rc.memory.roleName)) + ")");
        }
    });

    //creep behavior
    _.forEach(Game.creeps, creep => {
        if (creep.memory.goal) {
            delete creep.memory.idleSince; //creep is not idle if it is working on a goal
            runBehaviorByName(creep.memory.goal.behaviorName, creep, creep.memory.goal.goalArgs);
        }
        else
            BehaviorIdle.run(creep);
    });

    //statistics
    _.forEach(Game.rooms, room => {
        updateRoomStatistics(room);
        logRoomStatistics(room);
    });

    // Automatically delete memory of missing creeps
    for (const name in Memory.creeps) {
        if (!(name in Game.creeps)) {
            delete Memory.creeps[name];
        }
    }
});
