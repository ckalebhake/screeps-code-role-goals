import {moveToOptions} from "../utils/lib.creep";
import {idlePos, roomContainers, roomContainersAndStorage} from "../utils/lib.room";
import {behaviorLog, roomLog} from "../utils/lib.log";
import {BehaviorStoreResource} from "./BehaviorStoreResource";


/**
 * performs construction on existing sites
 */
export const BehaviorConstruct: Behavior =
    {
        name: "BehaviorConstruct",
        run(creep: Creep, constructionSiteId: string)
        {
            const targetSite = <ConstructionSite>Game.getObjectById(constructionSiteId);

            if (!targetSite)
            {
                const structures = creep.room.lookForAt(LOOK_STRUCTURES,
                    creep.memory.goal!.pos.x,
                    creep.memory.goal!.pos.y);

                if (structures && structures[0] && structures[0].hitsMax && structures[0].hits < structures[0].hitsMax)
                {
                    behaviorLog(creep.room.name, LogType.debugBehavior, this, "repairing new structure: " + structures[0]);
                    creep.memory.goal = {
                        name: "repair",
                        pos: structures[0].pos,
                        goalArgs: structures[0].id,
                        behaviorName: BehaviorRepair.name,
                        partsRequested: [{part: "work", min: 1}],
                        priority: GoalPriority.standard
                    };
                    return;
                }

                behaviorLog(creep.room.name, LogType.debugBehavior, this, "can't find target (" + constructionSiteId + ")");
                delete creep.memory.goal;
                return;
            }

            if (creep.build(targetSite) === ERR_NOT_IN_RANGE)
                creep.moveTo(targetSite, moveToOptions(this, 3));

            //if out of energy: drop the goal
            if (creep.carry.energy === 0)
                delete creep.memory.goal;
        }
    };

/**
 * repairs the target structure
 */
export const BehaviorRepair: Behavior =
    {
        name: "BehaviorRepair",
        run(creep: Creep, targetStructureId: string)
        {
            const targetStructure = <Structure>Game.getObjectById(targetStructureId);

            if (!targetStructure)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find target (" + targetStructureId + ")");
                delete creep.memory.goal;
                return;
            }

            if (creep.repair(targetStructure) === ERR_NOT_IN_RANGE)
                creep.moveTo(targetStructure, moveToOptions(this, 3));

            if (creep.carry.energy === 0)
                delete creep.memory.goal;
        }
    };

/**
 * collects resources from the target container or tombstone
 */
export const BehaviorCollectFromContainer: Behavior =
    {
        name: "BehaviorCollectFromContainer",
        run(creep: Creep, targetContainerId: string)
        {
            const targetContainer = <StructureContainer | Tombstone>Game.getObjectById(targetContainerId);

            if (!targetContainer)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find target (" + targetContainerId + ")");
                delete creep.memory.goal;
                return;
            }

            const targetResource: ResourceConstant = <ResourceConstant>_.max(_.keys(targetContainer.store), resource =>
            {
                //if the room has no storage, don't consider resources other than energy
                if (resource === RESOURCE_ENERGY || creep.room.storage)
                    return (targetContainer.store as any)[resource];
                else
                    return -1;
            });

            //@ts-ignore permit the following call even though Tombstone is not in the ts starter's list of valid structures
            const withdrawResult = creep.withdraw(targetContainer, targetResource);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_INVALID_ARGS: //this one can happen if the structure no longer has the target resource by the time we go to withdraw it
                    if (!<Tombstone>targetContainer)
                        behaviorLog(creep.room.name, LogType.warning, this, "invalid args(" + targetContainer + ", " + targetResource + ")");
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetContainer, moveToOptions(this));
                    break;
                default:
                    behaviorLog(creep.room.name, LogType.error, this, "unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (_.sum(targetContainer.store) === 0 || _.sum(creep.carry) === creep.carryCapacity || (!creep.room.storage && (targetContainer.store as any)[RESOURCE_ENERGY] === 0))
                delete creep.memory.goal;
        }
    };

/**
 * collects resources from the target container
 */
export const BehaviorCollectFromLab: Behavior =
    {
        name: "BehaviorCollectFromLab",
        run(creep: Creep, targetLabId: string)
        {
            const targetLab = <StructureLab>Game.getObjectById(targetLabId);

            if (!targetLabId)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find target (" + targetLabId + ", " + creep.memory!.goal!.goalArgs + ")");
                return;
            }

            //dont withdraw from an empty lab
            if (targetLab.mineralType === null)
            {
                delete creep.memory.goal;
                return;
            }

            const withdrawResult = creep.withdraw(targetLab, targetLab.mineralType);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetLab, moveToOptions(this));
                    break;
                default:
                    behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (targetLab.mineralAmount === 0 || _.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * collects energy from the room's storage structure
 */
export const BehaviorCollectEnergyFromStorage: Behavior =
    {
        name: "BehaviorCollectEnergyFromStorage",
        run(creep: Creep)
        {
            const storage = creep.room.storage;

            if (!storage)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "BehaviorCollectEnergyFromStorage can't find storage!  goal removed!");
                delete creep.memory.goal;
                return;
            }

            const withdrawResult = creep.withdraw(storage, RESOURCE_ENERGY);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(storage, moveToOptions(this));
                    break;
                case ERR_INVALID_ARGS:
                    behaviorLog(creep.room.name, LogType.warning, this, "invalid args(" + storage + ", " + RESOURCE_ENERGY + ")");
                    break;
                default:
                    behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (!storage.store.energy || _.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * collects energy from the room's terminal, making sure to leave Memory.globalSettings.desiredTerminalEnergyBalance inside for performing transactions
 */
export const BehaviorCollectEnergyFromTerminal: Behavior =
    {
        name: "BehaviorCollectEnergyFromTerminal",
        run(creep: Creep)
        {
            const terminal = creep.room.terminal;

            if (!terminal)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find terminal!  goal removed!");
                delete creep.memory.goal;
                return;
            }

            const withdrawAmount = (terminal.store.energy - Memory.globalSettings.desiredTerminalEnergyBalance);
            const withdrawResult = creep.withdraw(terminal, RESOURCE_ENERGY, withdrawAmount);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(terminal, moveToOptions(this));
                    break;
                case ERR_INVALID_ARGS:
                    behaviorLog(creep.room.name, LogType.warning, this, "invalid args(" + terminal + ", " + RESOURCE_ENERGY + ", " + withdrawAmount + ")");
                    break;
                default:
                    behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (terminal.store.energy <= Memory.globalSettings.desiredTerminalEnergyBalance || _.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * attacks the controller
 */
export const BehaviorAttackController: Behavior =
    {
        name: "BehaviorAttackController",
        run(creep: Creep)
        {
            if (creep.room.controller)
            {
                const attackResult = creep.attackController(creep.room.controller);
                switch (attackResult)
                {
                    case OK:
                    case ERR_TIRED:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.controller, moveToOptions(this));
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled attackController result " + attackResult);
                        break;
                }
            }
            else
            {
                behaviorLog(creep.room.name, LogType.warning, this, "no controller to attack!  Removing malformed goal");
                delete creep.memory.goal;
            }
        }
    };

/**
 * attacks the target creep
 */
export const BehaviorAttackCreep: Behavior =
    {
        name: "BehaviorAttackCreep",
        run(creep: Creep, creepId: string)
        {
            const targetCreep = <Creep>Game.getObjectById(creepId);
            if (targetCreep)
            {
                const attackResult = creep.attack(targetCreep);
                switch (attackResult)
                {
                    case OK:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(targetCreep, moveToOptions(this));
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled attack result.");
                        break;
                }
            }
            else
            {
                behaviorLog(creep.room.name, LogType.warning, this, "could not find target creep to attack.");
                delete creep.memory.goal;
            }
        }
    };

/**
 * claims the room
 */
export const BehaviorClaimRoom: Behavior =
    {
        name: "BehaviorClaimRoom",
        run(creep: Creep)
        {
            if (creep.room.controller)
            {
                const claimResult = creep.claimController(creep.room.controller);
                switch (claimResult)
                {
                    case OK:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.controller, moveToOptions(this));
                        break;
                    case ERR_INVALID_TARGET:
                        delete creep.memory.goal;
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled claimController result.");
                        break;
                }
            }
            else
            {
                behaviorLog(creep.room.name, LogType.warning, this, "no controller to attack!  Removing malformed goal");
                delete creep.memory.goal;
            }
        }
    };

/**
 * goes to the flag, registers itself with said flag, and then waits there, without removing the goal,
 * until an associated flag order tells the creep what to do next.
 */
export const BehaviorMuster: Behavior =
    {
        name: "BehaviorMuster",
        run(creep: Creep, targetFlagName: string)
        {
            if (!creep.memory.goal!.behaviorMemory)
                creep.memory.goal!.behaviorMemory = {};

            const flag: Flag = Game.flags[targetFlagName];
            if (!flag)
            {
                delete creep.memory.goal;
                return;
            }

            if (!creep.memory.goal!.behaviorMemory.arrivedAtRallyPoint)
            {
                if (creep.pos.getRangeTo(Game.flags[targetFlagName]) > 2)
                {
                    creep.moveTo(Game.flags[targetFlagName], moveToOptions(this));
                }
                else
                {
                    creep.memory.goal!.behaviorMemory = {arrivedAtRallyPoint: true};

                    if (!flag.memory.flagCreeps)
                        flag.memory.flagCreeps = [];

                    flag.memory.flagCreeps.push(creep.name);
                }
            }
        }
    };

/**
 * sends the creep to the given position
 */
export const BehaviorGoToPosition: Behavior =
    {
        name: "BehaviorGoToPosition",
        run(creep: Creep)
        {
            //extracting figures because what is stored in memory isnt an entirely valid roomPosition object
            creep.moveTo(creep.memory.goal!.pos.x, creep.memory.goal!.pos.y, moveToOptions(this));

            if (!creep.memory.goal!.behaviorMemory)
                creep.memory.goal!.behaviorMemory = {};

            //if moved into another room, go towards the center so we dont blink back and then clear the goal
            if (creep.pos.roomName !== creep.memory.goal!.pos.roomName)
            {
                if (!creep.memory.goal!.behaviorMemory.movedFromExit)
                {
                    creep.moveTo(25, 25, moveToOptions(this));
                    creep.memory.goal!.behaviorMemory.movedFromExit = true;
                }
                else
                {
                    delete creep.memory.goal;
                }
            }
            else if (creep.pos.isEqualTo(creep.memory.goal!.pos.x, creep.memory.goal!.pos.y))
            {
                //if at the target, clear goal
                delete creep.memory.goal;
            }
        }
    };

/**
 * sends the creep to the given room
 */
export const BehaviorGoToRoom: Behavior =
    {
        name: "BehaviorGoToRoom",
        run(creep: Creep, roomName: string)
        {
            if (creep.room.name === roomName)
            {
                //we are in the room!
                if (creep.pos.x === 0 ||
                    creep.pos.y === 0 ||
                    creep.pos.x === 49 ||
                    creep.pos.y === 49)
                {
                    creep.moveTo(25, 25, moveToOptions(this)); //we are on an exit: move towards the center of the room
                    return;
                }
                else
                {
                    //goal complete
                    delete creep.memory.goal;
                    return;
                }
            }
            else
            {
                //find a route to the target room
                creep.moveTo(new RoomPosition(25, 25, roomName));
            }
        }
    };

/**
 * picks up a dropped resource pile
 */
export const BehaviorCleanupDroppedResources: Behavior =
    {
        name: "BehaviorCleanupDroppedResources",
        run(creep: Creep, targetId: string)
        {
            const target = <Resource>Game.getObjectById(targetId);
            const pickupResult = creep.pickup(target);
            switch (pickupResult)
            {
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(target, moveToOptions(this));
                    break;
                case ERR_FULL:
                case OK:
                case ERR_INVALID_TARGET:
                    delete creep.memory.goal;
                    break;
                default:
                    behaviorLog(creep.room.name, LogType.warning, this, "Unknown pickup result: " + pickupResult);
                    break;
            }
        }
    };

/**
 * responsible for transporting minerals from one room to another.  This takes a flag, and uses the following from that flag:
 * pos: the creep looks for a building at the flag's position to store the minerals into
 * target: the creep uses the flag's target as the mineral to transport from the original room
 * the creep will collect the target mineral from its own room and carry as much as it can to the target lab.
 * If that lab is full, the creep stores them in the room's storage structure instead, if it has one.
 */
export const BehaviorTransportMinerals: Behavior =
    {
        name: "BehaviorTransportMinerals",
        run(creep: Creep, requestingFlagName: string)
        {
            if (!creep.memory.spawnedFromRoom)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "does not know what room it came from!");
                return;
            }

            const requestingFlag = Game.flags[requestingFlagName];
            if (!requestingFlag)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "a transport had it's destination flag removed.  Releasing for general service");
                delete creep.memory.goal;
            }

            if (!creep.memory.goal!.behaviorMemory)
                creep.memory.goal!.behaviorMemory = {state: "COLLECTING"};

            const targetMineral: ResourceConstant = <ResourceConstant>requestingFlag.memory.target;
            if (!targetMineral)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "invalid target mineral: " + requestingFlag.memory.target);
                return;
            }

            //fill up with minerals
            if (creep.memory.goal!.behaviorMemory.state === "COLLECTING")
            {
                if (creep.room.name !== creep.memory.spawnedFromRoom)
                {
                    //if not in the room we were spawned from, go there
                    creep.moveTo(new RoomPosition(25, 25, creep.memory.spawnedFromRoom), moveToOptions(this));
                }
                else
                {
                    //if in the room, collect minerals
                    const structuresWithTargetMineral: (StructureContainer | StructureStorage)[] =
                        roomContainersAndStorage(creep.room).filter(structure => (structure.store as any)[targetMineral]);
                    const targetStructure = creep.pos.findClosestByPath(structuresWithTargetMineral);

                    if (!targetStructure)
                    {
                        creep.memory.goal!.behaviorMemory.state = "TRANSPORTING";
                        return;
                    }

                    const withdrawResult = creep.withdraw(targetStructure, targetMineral);
                    switch (withdrawResult)
                    {
                        case OK:
                        case ERR_FULL:
                        case ERR_NOT_ENOUGH_RESOURCES:
                            break;
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(targetStructure, moveToOptions(this));
                            break;
                        default:
                            behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                            break;
                    }

                    if (_.sum(creep.carry) === creep.carryCapacity)
                        creep.memory.goal!.behaviorMemory.state = "TRANSPORTING";
                }
            }

            if (creep.memory.goal!.behaviorMemory.state === "TRANSPORTING")
            {
                //transport minerals
                const targetLab: StructureLab = <StructureLab>_.find(requestingFlag.pos.lookFor(LOOK_STRUCTURES), s => s.structureType === STRUCTURE_LAB);

                let targetStructure: StructureLab | StructureStorage;
                let transferAmount: number;

                if (targetLab.mineralAmount === targetLab.mineralCapacity && creep.room.storage)
                {
                    targetStructure = (creep.room.storage);
                    transferAmount = Math.min((creep.carry[targetMineral] || 0), creep.room.storage.storeCapacity - _.sum(creep.room.storage.store))
                }
                else
                {
                    targetStructure = targetLab;
                    transferAmount = Math.min((creep.carry[targetMineral] || 0), targetLab.mineralCapacity - targetLab.mineralAmount);

                    //since this is a lab, ensure a multiple of 15
                    if (((targetLab.mineralAmount + transferAmount) % 15) > 0)
                        transferAmount -= ((targetLab.mineralAmount + transferAmount) % 15);
                }

                if (!targetStructure)
                {
                    behaviorLog(creep.room.name, LogType.warning, this, "nowhere to store transported minerals!");
                    return;
                }

                const transferResult = creep.transfer(targetStructure, targetMineral, transferAmount);
                switch (transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(targetStructure, moveToOptions(this));
                        break;

                    case OK:
                    case ERR_NOT_ENOUGH_RESOURCES:
                        if ((creep.carry[targetMineral] || 0) < 15)
                            creep.memory.goal!.behaviorMemory.state = "COLLECTING";
                        break;

                    case ERR_FULL:
                        break;

                    case ERR_INVALID_ARGS:
                        behaviorLog(creep.room.name, LogType.warning, this, "used invalid args! " + targetStructure + ", " + targetMineral + ", " + transferAmount);
                        // creep.memory.goal!.behaviorMemory.state = "COLLECTING";
                        break;

                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "Unhandled transfer result: " + transferResult);
                        break;
                }
            }

        }
    };

/**
 * sells value units of resource resource at whatever the current market price is.
 * If there are open buy orders, this makes a deal with them.  If not, this undercuts the price of an existing sell order.
 */
export const BehaviorSellResourceAtAnyPrice: Behavior =
    {
        name: "BehaviorSellResourceAtAnyPrice",
        run(creep: Creep, goalArgs: {resource: ResourceConstant, value: number})
        {
            const terminal = creep.room.terminal;

            if (!terminal)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find a terminal!");
                delete creep.memory.goal;
                return;
            }

            if (!creep.room.memory.sellAtAnyPrice || creep.room.memory.sellAtAnyPrice.length === 0 || creep.room.memory.sellAtAnyPrice[0].resource !== goalArgs.resource)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "trade was invalidated");
                delete creep.memory.goal;
                return;
            }

            const amountStillNeeded = goalArgs.value - (terminal.store[goalArgs.resource] || 0);

            const availableTerminalCapacity = (terminal.storeCapacity - _.sum(terminal.store));

            if (availableTerminalCapacity < amountStillNeeded)
            {
                creep.room.memory.sellAtAnyPrice![0].amount = availableTerminalCapacity;
                creep.memory.goal!.goalArgs.value = (terminal.store[goalArgs.resource] || 0) + availableTerminalCapacity;
                behaviorLog(creep.room.name, LogType.warning, this, "shrank a sellAtAnyPrice request because the terminal did not have room");
            }

            behaviorLog(creep.room.name, LogType.market, this, "selling " + goalArgs.resource + " at any price. (" + amountStillNeeded + " remaining to transport)");

            if (amountStillNeeded <= 0)
            {
                //terminal is ready to trade
                const buyOrdersForResource = Game.market.getAllOrders({resourceType: goalArgs.resource, type: ORDER_BUY});
                if (!buyOrdersForResource || buyOrdersForResource.length === 0)
                {
                    behaviorLog(creep.room.name, LogType.market, this, "no buy orders for " + goalArgs.resource + ". Creating a sell order instead.");
                    const existingSellOrders = Game.market.getAllOrders({resourceType: goalArgs.resource, type: ORDER_SELL});
                    existingSellOrders.sort(o => o.price);
                    if (existingSellOrders.length === 0)
                    {
                        behaviorLog(creep.room.name, LogType.market, this, "couldn't do that either: there are no sell orders to compare it to!");
                        return;
                    }
                    const sellPrice = existingSellOrders[existingSellOrders.length - 1].price;
                    Game.market.createOrder(ORDER_SELL, goalArgs.resource, sellPrice - 0.001, goalArgs.value, creep.room.name);
                    creep.room.memory.sellAtAnyPrice!.splice(0, 1);

                    delete creep.memory.goal;
                    return;
                }

                buyOrdersForResource.sort(o => o.price);
                const tradeAmount = Math.min(goalArgs.value, buyOrdersForResource[0].remainingAmount);
                const dealResult = Game.market.deal(buyOrdersForResource[0].id, tradeAmount, creep.room.name);
                switch (dealResult)
                {
                    case OK:
                        creep.room.memory.sellAtAnyPrice![0].amount -= tradeAmount;
                        if (creep.room.memory.sellAtAnyPrice![0].amount <= 0)
                            creep.room.memory.sellAtAnyPrice!.splice(0, 1);
                        roomLog(creep.room.name, LogType.market, "Completed sellAtAnyPrice for " + goalArgs.resource);
                        delete creep.memory.goal;
                        break;
                    case ERR_TIRED:
                        break;
                    case ERR_NOT_ENOUGH_RESOURCES:
                        roomLog(creep.room.name, LogType.market, "insufficient resources to complete trade!");
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled deal result " + dealResult);
                        break;
                }
            }
            else if (creep.carry[goalArgs.resource])
            {
                //put held resource into the terminal
                const transferResult = creep.transfer(terminal, goalArgs.resource, Math.min(creep.carry[goalArgs.resource]!, amountStillNeeded));
                switch (transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(terminal, moveToOptions(this));
                        break;

                    case OK:
                    case ERR_NOT_ENOUGH_RESOURCES:
                    case ERR_FULL:
                        break;

                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "Unhandled transfer result: " + transferResult);
                        break;
                }
            }
            else if (creep.room.storage && creep.room.storage.store[goalArgs.resource])
            {
                //collect resource from storage
                const withdrawResult = creep.withdraw(creep.room.storage, goalArgs.resource, Math.min((creep.carryCapacity - _.sum(creep.carry)), amountStillNeeded));
                switch (withdrawResult)
                {
                    case OK:
                    case ERR_FULL:
                    case ERR_NOT_ENOUGH_RESOURCES:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.storage, moveToOptions(this));
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                        break;
                }
            }
            else
            {
                //resource unavailable
                behaviorLog(creep.room.name, LogType.warning, this, "can't find any stored " + goalArgs.resource + " to trade with!");
                delete creep.memory.goal;
            }
        }
    };

/**
 * transfers units of resource resource to the destination room
 */
export const BehaviorTransferResourceToRoom: Behavior =
    {
        name: "BehaviorTransferResourceToRoom",
        run(creep: Creep, goalArgs: {target: string, value: number})
        {
            const terminal = creep.room.terminal;

            const targetParts: string[] = goalArgs.target.split("::");

            const resource = targetParts[0];
            const targetRoom = targetParts[1];

            if (!resource || !targetRoom)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "was cancelled because of an invalid target string.  Should be format resource::roomName");
                delete creep.memory.goal;
            }

            if (!terminal)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find a terminal!");
                delete creep.memory.goal;
                return;
            }

            if (!creep.room.memory.transferResources || creep.room.memory.transferResources.length === 0 || creep.room.memory.transferResources[0].resource !== resource)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "transfer was invalidated");
                delete creep.memory.goal;
                return;
            }

            const amountStillNeeded = goalArgs.value - (terminal.store[resource] || 0);

            const availableTerminalCapacity = (terminal.storeCapacity - _.sum(terminal.store));

            if (availableTerminalCapacity < amountStillNeeded)
            {
                creep.room.memory.transferResources![0].amount = availableTerminalCapacity;
                creep.memory.goal!.goalArgs.value = (terminal.store[resource] || 0) + availableTerminalCapacity;
                behaviorLog(creep.room.name, LogType.warning, this, "shrank a transferResources request because the terminal did not have room");
            }

            behaviorLog(creep.room.name, LogType.market, this, "transferring " + resource + " to room " + targetRoom + " (" + amountStillNeeded + " remaining to transport)");

            if (amountStillNeeded <= 0)
            {
                //terminal is ready to transfer
                const transferResult = terminal.send(resource, goalArgs.value, targetRoom, "Transfer from " + creep.room.name);

                switch (transferResult)
                {
                    case OK:
                        creep.room.memory.transferResources!.splice(0, 1);
                        behaviorLog(creep.room.name, LogType.warning, this, "Completed transfer of " + resource + " to room " + targetRoom);
                        delete creep.memory.goal;
                        break;
                    case ERR_TIRED:
                        break;
                    case ERR_NOT_ENOUGH_RESOURCES:
                        behaviorLog(creep.room.name, LogType.warning, this, "insufficient resources to complete transfer in " + creep.room.name + "!");
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled transfer result " + transferResult);
                        break;
                }
            }
            else if (creep.carry[resource])
            {
                //put held resource into the terminal
                const transferResult = creep.transfer(terminal, resource, Math.min(creep.carry[resource]!, amountStillNeeded));
                switch (transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(terminal, moveToOptions(this));
                        break;

                    case OK:
                    case ERR_NOT_ENOUGH_RESOURCES:
                    case ERR_FULL:
                        break;

                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "Unhandled transfer result: " + transferResult);
                        break;
                }
            }
            else if (creep.room.storage && creep.room.storage.store[resource])
            {
                //collect resource from storage
                const withdrawResult = creep.withdraw(creep.room.storage, resource, Math.min((creep.carryCapacity - _.sum(creep.carry)), amountStillNeeded));
                switch (withdrawResult)
                {
                    case OK:
                    case ERR_FULL:
                    case ERR_NOT_ENOUGH_RESOURCES:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.storage, moveToOptions(this));
                        break;
                    default:
                        behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                        break;
                }
            }
            else
            {
                //resource unavailable
                behaviorLog(creep.room.name, LogType.warning, this, "can't find any stored " + resource + " to transfer!");
                delete creep.memory.goal;
            }
        }
    };

/**
 * special behavior that is run on creeps that have no goal assigned.
 * no goal should ever use this behavior!
 */
export const BehaviorIdle: Behavior =
    {
        name: "BehaviorIdle",
        run(creep: Creep)
        {
            //idle creeps are willing to share
            creep.memory.isWillingToShareEnergy = true;

            //fallback code to try and prevent creeps from sitting around with inventories full of minerals when storage is down
            if (_.sum(creep.carry) > creep.carry.energy)
            {
                //idle creep is holding non-energy resources.  this is probably a bad sign...
                if (creep.room.storage)
                {
                    console.log(Game.time + " [" + creep.room.name + "][WARN] an idle creep is holding minerals even though a storage structure exists!  This probably means something is wrong with goal selection and/or assignment");
                }
                else
                {
                    //there is no storage.  Look for a container to use instead
                    const containers = roomContainers(creep.room).filter(container => (container.storeCapacity - _.sum(container.store)) > 200);
                    if (containers.length === 0)
                    {
                        behaviorLog(creep.room.name, LogType.warning, this, "creeps have nowhere to store minerals!");
                    }
                    else
                    {
                        const target = containers.sort(container => container.storeCapacity - _.sum(container.store))[0];
                        creep.memory.goal =
                            {
                                name: "Store minerals in container",
                                pos: target.pos,
                                priority: GoalPriority.standard,
                                goalArgs: target.id,
                                behaviorName: BehaviorStoreResource.name,
                                partsRequested: [],
                            };
                        return;
                    }
                }
            }

            //track how long unit has been idle
            if (!creep.memory.idleSince)
                creep.memory.idleSince = Game.time;

            //reasons for a creep to suicide/recycle itself
            if ((!creep.memory.specialized && (Game.time - creep.memory.idleSince) >= 100) || //unspecialized creeps that are idle for 100 ticks
                (_.every(creep.body, bpd => bpd.hits === 0 || bpd.type === MOVE)))    //creeps where all working parts are MOVE
            {
                const roomSpawn = creep.room.find(FIND_MY_SPAWNS)[0];
                if (!roomSpawn)
                {
                    behaviorLog(creep.room.name, LogType.warning, this, "creep " + creep.name + " has committed suicide.");
                    creep.suicide();
                }
                else
                {
                    creep.say("Recycling");
                    if (roomSpawn.recycleCreep(creep) === ERR_NOT_IN_RANGE)
                        creep.moveTo(roomSpawn, moveToOptions(this));
                }
            }
            else
            {
                creep.say("ZZZ");
                const idleFlag = _.find(Game.flags, flag => (flag.pos.roomName === creep.room.name));
                if (idleFlag)
                    creep.moveTo(idleFlag, moveToOptions(this));
                else
                    creep.moveTo(idlePos(creep.room), moveToOptions(this));
            }
        }
    };

/**
 * responsible for transporting minerals from one room to another.  This takes a flag, and uses the following from that flag:
 * pos: the creep looks for a building at the flag's position to store the minerals into
 * target: the creep uses the flag's target as the mineral to transport from the original room
 * the creep will collect the target mineral from its own room and carry as much as it can to the target lab.
 * If that lab is full, the creep stores them in the room's storage structure instead, if it has one.
 */
export const BehaviorTransportEnergy: Behavior =
    {
        name: "BehaviorTransportEnergy",
        run(creep: Creep)
        {
            if (!creep.memory.spawnedFromRoom)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "does not know what room it came from!");
                return;
            }

            const requestingFlag = new RoomPosition(creep.memory.goal!.pos.x, creep.memory.goal!.pos.y, creep.memory.goal!.pos.roomName).lookFor(LOOK_FLAGS)[0];
            if (!requestingFlag)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "a transport had it's destination flag removed.  Releasing for general service");
                delete creep.memory.goal;
            }

            if (!creep.memory.goal!.behaviorMemory)
                creep.memory.goal!.behaviorMemory = {state: "COLLECTING"};

            //fill up with energy
            if (creep.memory.goal!.behaviorMemory.state === "COLLECTING")
            {
                if (creep.room.name !== creep.memory.goal!.pos.roomName)
                {
                    //if not in the room the goal is in, go there
                    creep.moveTo(new RoomPosition(25, 25, creep.memory.goal!.pos.roomName), moveToOptions(this));
                }
                else
                {
                    if (!creep.memory.goal!.behaviorMemory.containerIds)
                        creep.memory.goal!.behaviorMemory.containerIds = roomContainers(creep.room).map(c => c.id);

                    if (creep.memory.goal!.behaviorMemory.containerIds.length === 0)
                    {
                        behaviorLog(creep.room.name, LogType.warning, this, "no containers present in remote harvesting room " + creep.room.name);
                        const idleFlag = _.find(Game.flags, flag => flag.pos.roomName === creep.room.name);
                        if (idleFlag)
                            creep.moveTo(idleFlag, moveToOptions(this));
                        else
                            creep.moveTo(25, 25, moveToOptions(this));
                    }

                    const containers: StructureContainer[] = creep.memory.goal!.behaviorMemory.containerIds.map(((id: string) => Game.getObjectById(id)));

                    const targetContainerIndex = creep.memory.goal!.behaviorMemory.containerIndex || 0;
                    const targetContainer = containers[targetContainerIndex];

                    const withdrawResult = creep.withdraw(targetContainer, RESOURCE_ENERGY);
                    switch (withdrawResult)
                    {
                        case OK:
                        case ERR_FULL:
                            creep.memory.goal!.behaviorMemory.containerIndex = (targetContainerIndex + 1) % containers.length;
                            break;
                        case ERR_NOT_ENOUGH_RESOURCES:
                            break;
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(targetContainer, moveToOptions(this));
                            break;
                        case ERR_INVALID_TARGET:
                            behaviorLog(creep.room.name, LogType.warning, this, "resetting container memory because one of the containers disappeared.");
                            delete creep.memory.goal!.behaviorMemory.containerIds;
                            delete creep.memory.goal!.behaviorMemory.containerIndex;
                            break;
                        default:
                            behaviorLog(creep.room.name, LogType.warning, this, "unhandled withdraw result: " + withdrawResult);
                            break;
                    }

                    if (_.sum(creep.carry) === creep.carryCapacity)
                        creep.memory.goal!.behaviorMemory.state = "TRANSPORTING";
                }
            }

            if (creep.memory.goal!.behaviorMemory.state === "TRANSPORTING")
            {
                if (creep.room.name !== creep.memory.spawnedFromRoom)
                {
                    creep.moveTo(new RoomPosition(25, 25, creep.memory.spawnedFromRoom), moveToOptions(this));
                }
                else
                {
                    //transport energy
                    if (!creep.room.storage)
                    {
                        behaviorLog(creep.room.name, LogType.warning, this, "nowhere to store transported energy!");
                        return;
                    }

                    const transferResult = creep.transfer(creep.room.storage, RESOURCE_ENERGY);
                    switch (transferResult)
                    {
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(creep.room.storage, moveToOptions(this));
                            break;
                        case OK:
                        case ERR_NOT_ENOUGH_RESOURCES:
                            break;
                        case ERR_FULL:
                            break;
                        case ERR_INVALID_ARGS:
                            console.log(this.name + " used invalid args!", creep.room.storage, RESOURCE_ENERGY);
                            break;
                        default:
                            console.log(this.name + " Unhandled transfer result: " + transferResult);
                            break;
                    }

                    if (creep.carry.energy === 0)
                        creep.memory.goal!.behaviorMemory.state = "COLLECTING";
                }
            }
        }
    };
