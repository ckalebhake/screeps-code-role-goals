import {moveToOptions} from "../utils/lib.creep";

/**
 * stores held resources in a container or storage.  Requires a target.
 */
export const BehaviorStoreResource: Behavior =
    {
        name: "BehaviorStoreResource",
        run(creep: Creep, targetContainerOrStorageId: string)
        {
            const container: StructureContainer | StructureStorage = <StructureContainer | StructureStorage>Game.getObjectById(targetContainerOrStorageId);
            if (!container)
            {
                console.log(Game.time + "[" + creep.room.name + "][ERROR] Could not find target container or storage!");
                return;
            }

            const targetResource: ResourceConstant = <ResourceConstant>_.max(_.keys(creep.carry), resource =>
            {
                return (creep.carry as any)[resource];
            });

            const transferResult = creep.transfer(container, targetResource, creep.carry[targetResource]);
            switch (transferResult)
            {
                //too far away from target
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(container, moveToOptions(this));
                    break;

                //success
                case OK:
                    break;

                //target is full
                case ERR_FULL:
                    if ((targetResource !== RESOURCE_ENERGY) && creep.room.storage)
                    {
                        //if we are carrying something that isn't energy and there is a storage structure, fall back to that instead.
                        creep.memory.goal!.goalArgs = creep.room.storage.id;
                        break;
                    }
                    else
                    {
                        //otherwise, clear the goal.
                        delete creep.memory.goal;
                        return;
                    }

                //we dont actually have the resources to perform the transfer
                case ERR_NOT_ENOUGH_RESOURCES:
                    delete creep.memory.goal;
                    return;

                default:
                    console.log("Unhandled transfer result: " + transferResult);
                    break;
            }

            if (creep.carry.energy === 0)
            {
                delete creep.memory.goal;
            }
        }
    };