import {behaviorLog} from "../utils/lib.log";
import {roomContainersAndStorage} from "../utils/lib.room";
import {moveToOptions} from "../utils/lib.creep";

export const BehaviorSupplyStructure: Behavior =
    {
        name: "BehaviorSupplyStructure",
        run (creep: Creep, targetStructureId: string)
        {
            const targetStructure = Game.getObjectById(targetStructureId) as SuppliableStructure;
            if(!targetStructure)
            {
                behaviorLog(creep.room.name, LogType.error, this, " unable to find target structure " + targetStructureId + ", or it is not a suppliable structure!");
                return;
            }

            switch(creep.transfer(targetStructure, "energy"))
            {
                case OK:
                case ERR_FULL:
                    delete creep.memory.goal;
                    return;

                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetStructure, moveToOptions(this));
                    return;

                default: behaviorLog(creep.room.name, LogType.warning, this, "unhandled transfer result!");
            }
        }
    };

/**
 * transfers energy the creep is holding to a spawn or spawn extension
 */
export const BehaviorSupplySpawnOrExtension: Behavior =
    {
        name: "BehaviorSupplySpawnOrExtension",
        run(creep: Creep, targetStructureId: string)
        {
            const targetStructure = <StructureSpawn | StructureExtension>Game.getObjectById(targetStructureId);

            if (!targetStructure)
            {
                console.log("[ERROR]: BehaviorSupplySpawnOrExtension can't find target (" + targetStructureId + ")");
                return;
            }

            const transferResult: ScreepsReturnCode =
                creep.transfer(targetStructure,
                    RESOURCE_ENERGY,
                    Math.min(creep.carry.energy, targetStructure!.energyCapacity - targetStructure!.energy));

            switch (transferResult)
            {
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetStructure, moveToOptions(this));

                    if (creep.carry.energy === 0)
                        delete creep.memory.goal;
                    break;

                case ERR_NOT_ENOUGH_RESOURCES:
                case ERR_FULL:
                    delete creep.memory.goal;
                    break;

                case OK:
                    break;

                default:
                    console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.transfer error: " + transferResult.toString());
            }
        }
    };
/**
 * transfers energy the creep is holding to a terminal, but only bringing the balance up to Memory.globalSettings.desiredTerminalEnergyBalance
 */
export const BehaviorSupplyTerminal: Behavior =
    {
        name: "BehaviorSupplyTerminal",
        run(creep: Creep)
        {
            const terminal = creep.room.terminal;

            if (!terminal)
            {
                console.log("[ERROR]: BehaviorSupplyTerminal can't find terminal");
                delete creep.memory.goal;
                return;
            }

            if (terminal.store.energy > Memory.globalSettings.desiredTerminalEnergyBalance)
                delete creep.memory.goal;

            const transferResult: ScreepsReturnCode =
                creep.transfer(terminal,
                    RESOURCE_ENERGY,
                    Math.min(creep.carry.energy, Memory.globalSettings.desiredTerminalEnergyBalance - terminal.store.energy));

            switch (transferResult)
            {
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(terminal, moveToOptions(this));

                    if (creep.carry.energy === 0)
                        delete creep.memory.goal;
                    break;

                case ERR_NOT_ENOUGH_RESOURCES:
                case ERR_FULL:
                case OK:
                    delete creep.memory.goal;
                    break;

                default:
                    console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.transfer error: " + transferResult.toString());
            }
        }
    };

/**
 * supplies a lab with the specified mineral
 */
export const BehaviorSupplyLabWithMineral: Behavior =
    {
        name: "BehaviorSupplyLabWithMineral",
        run(creep: Creep, targetMineral: ResourceConstant)
        {
            const targetLab: StructureLab = <StructureLab>_.find(creep.room.lookForAt(LOOK_STRUCTURES, creep.memory.goal!.pos.x, creep.memory.goal!.pos.y), s => s.structureType === STRUCTURE_LAB);
            if (!targetLab)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "can't find a lab at the goal position");

                delete creep.memory.goal;
                return;
            }
            if (!targetMineral)
            {
                behaviorLog(creep.room.name, LogType.warning, this, "was not given a target mineral");

                delete creep.memory.goal;
                return;
            }

            if (!creep.memory.goal!.behaviorMemory)
                creep.memory.goal!.behaviorMemory = {holdingTargetMineral: false};

            if (!creep.memory.goal!.behaviorMemory.holdingTargetMineral)
            {
                //obtain the mineral to deposit
                if (creep.carry[targetMineral])
                {
                    //we already have it.  skip this step.
                    creep.memory.goal!.behaviorMemory.holdingTargetMineral = true;
                    return;
                }

                const structuresWithTargetMineral = roomContainersAndStorage(creep.room).filter(s => s.store[targetMineral]);
                const closestStructureWithTargetMIneral = creep.pos.findClosestByPath(structuresWithTargetMineral);

                if (closestStructureWithTargetMIneral)
                {
                    //withdraw minerals

                    //maximum amount we could withdraw
                    let transferSize = Math.min(creep.carryCapacity - _.sum(creep.carry), closestStructureWithTargetMIneral.store[targetMineral]!);

                    //try to withdraw an amount that would bring the lab to a multiple of 15
                    if (((targetLab.mineralAmount + transferSize) % 15) !== 0)
                        transferSize -= targetLab.mineralAmount % 15;

                    const withdrawResult: ScreepsReturnCode =
                        creep.withdraw(closestStructureWithTargetMIneral, targetMineral,);

                    switch (withdrawResult)
                    {
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(closestStructureWithTargetMIneral, moveToOptions(this));
                            break;

                        case ERR_NOT_ENOUGH_RESOURCES:
                        case ERR_FULL:
                            delete creep.memory.goal;
                            break;

                        case OK:
                            creep.memory.goal!.behaviorMemory.holdingTargetMineral = true;
                            break;

                        default:
                            behaviorLog(creep.room.name, LogType.error, this, ": unhandled creep.withdraw error: " + withdrawResult.toString());
                    }
                }
            }
            else
            {
                //deposit held mineral into lab

                //maximum possible transfer
                let transferSize = Math.min(creep.carry[targetMineral]!, targetLab.mineralCapacity - targetLab.mineralAmount);

                //only transfer an amount that would bring the lab to a multiple of 15
                if (((targetLab.mineralAmount + transferSize) % 15) !== 0)
                    transferSize -= targetLab.mineralAmount % 15;

                const transferResult: ScreepsReturnCode = creep.transfer(targetLab, targetMineral, transferSize);

                switch (transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(targetLab, moveToOptions(this));

                        if (!creep.carry[targetMineral])
                        {
                            delete creep.memory.goal;
                        }

                        break;

                    case ERR_NOT_ENOUGH_RESOURCES:
                    case ERR_FULL:
                        delete creep.memory.goal;
                        break;

                    case OK:
                    case ERR_INVALID_ARGS:
                        if ((creep.carry[targetMineral] || 0) < 15)
                        {
                            delete creep.memory.goal;
                        }

                        break;

                    default:
                        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.transfer error: " + transferResult.toString());
                }
            }
        }
    };