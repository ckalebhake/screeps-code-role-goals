import {
    BehaviorAttackController,
    BehaviorAttackCreep,
    BehaviorClaimRoom,
    BehaviorCleanupDroppedResources,
    BehaviorCollectEnergyFromStorage,
    BehaviorCollectEnergyFromTerminal,
    BehaviorCollectFromContainer,
    BehaviorCollectFromLab,
    BehaviorConstruct,
    BehaviorGoToPosition,
    BehaviorGoToRoom,
    BehaviorMuster,
    BehaviorRepair,
    BehaviorSellResourceAtAnyPrice,
    BehaviorTransferResourceToRoom,
    BehaviorTransportEnergy,
    BehaviorTransportMinerals
} from "./MiscBehaviors";
import {BehaviorCollectEnergy, BehaviorHarvestEnergy, BehaviorHarvestToContainer} from "./HarvestBehaviors";
import {BehaviorUpgrade} from "./BehaviorUpgrade";
import {BehaviorStoreResource} from "./BehaviorStoreResource";
import {
    BehaviorSupplyLabWithMineral,
    BehaviorSupplySpawnOrExtension,
    BehaviorSupplyStructure,
    BehaviorSupplyTerminal
} from "./SupplyBehaviors";
import {globalLog} from "../utils/lib.log";

/**
 * runs the behavior with the given name
 * @param {string} name name of the behavior
 * @param {Creep} creep creep to act
 * @param goalArgs arguments to pass to the goal
 */
export function runBehaviorByName(name: string, creep: Creep, goalArgs?: any): void
{
    const nameMap: any =
        {
            "BehaviorAttackController":             BehaviorAttackController,
            "BehaviorAttackCreep":                  BehaviorAttackCreep,
            "BehaviorClaimRoom":                    BehaviorClaimRoom,
            "BehaviorCleanupDroppedResources":      BehaviorCleanupDroppedResources,
            "BehaviorCollectEnergy":                BehaviorCollectEnergy,
            "BehaviorCollectEnergyFromStorage":     BehaviorCollectEnergyFromStorage,
            "BehaviorCollectEnergyFromTerminal":    BehaviorCollectEnergyFromTerminal,
            "BehaviorCollectFromContainer":         BehaviorCollectFromContainer,
            "BehaviorCollectFromLab":               BehaviorCollectFromLab,
            "BehaviorConstruct":                    BehaviorConstruct,
            "BehaviorGoToPosition":                 BehaviorGoToPosition,
            "BehaviorGoToRoom":                     BehaviorGoToRoom,
            "BehaviorHarvestEnergy":                BehaviorHarvestEnergy,
            "BehaviorHarvestToContainer":           BehaviorHarvestToContainer,
            "BehaviorMuster":                       BehaviorMuster,
            "BehaviorRepair":                       BehaviorRepair,
            "BehaviorSellResourceAtAnyPrice":       BehaviorSellResourceAtAnyPrice,
            "BehaviorStoreResource":                BehaviorStoreResource,
            "BehaviorSupplyLabWithMineral":         BehaviorSupplyLabWithMineral,
            "BehaviorSupplySpawnOrExtension":       BehaviorSupplySpawnOrExtension,
            "BehaviorSupplyStructure":              BehaviorSupplyStructure,
            "BehaviorSupplyTerminal":               BehaviorSupplyTerminal,
            "BehaviorTransferResourceToRoom":       BehaviorTransferResourceToRoom,
            "BehaviorTransportMinerals":            BehaviorTransportMinerals,
            "BehaviorTransportEnergy":              BehaviorTransportEnergy,
            "BehaviorUpgrade":                      BehaviorUpgrade,
        };

    const behavior: Behavior = <Behavior>nameMap[name];
    if (behavior)
    {
        if(!creep.spawning) //don't ask creeps to do things if they dont exist yet
        {
            globalLog(LogType.trace, " > " + behavior.name);
            behavior.run(creep, goalArgs);
            globalLog(LogType.trace, " < " + behavior.name);
        }
    }
    else
    {
        console.log(Game.time + "[" + creep.room.name + "][ERROR]: could not find behavior " + name + ".  Removing malformed goal on creep " + creep.name);
        delete creep.memory.goal;
    }
}