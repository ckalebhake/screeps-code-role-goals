/**
 * convenience function for logging events
 * @param logType  type of log message
 * @param message  the message
 */
export const globalLog = function (logType: LogType, message: string)
{
    if (Memory.globalSettings.enabledLogTypes.includes(logType))
        console.log(Game.time + " | " + logType + " | Global | " + message);
};

/**
 * convenience function for logging events pertaining to a specific room
 * @param roomName name of the room
 * @param logType  type of log message
 * @param message  the message
 */
export const roomLog = function (roomName: string, logType: LogType, message: string)
{
    if(Memory.rooms[roomName].enabledLogTypes.includes(logType))
        console.log(Game.time + " | " + logType + " | " + roomName + " | " + message);
};

/**
 * convenience function for logging events pertaining to a specific behavior
 * @param roomName name of the room
 * @param logType  type of log message
 * @param behavior the behavior logging the message
 * @param message  the message
 */
export const behaviorLog = function (roomName: string, logType: LogType, behavior: Behavior, message: string)
{
    if(Memory.rooms[roomName].enabledLogTypes.includes(logType))
        console.log(Game.time + " | " + logType + " | " + roomName + " | " + behavior.name + " | " + message);
};

