/**
 * returns all creeps assigned to this role that are still alive
 * also purges the list of creeps that no longer exist
 * @param roleMemory
 */
export function getRoleCreeps(roleMemory: RoleState): Creep[]
{
    //remove missing creeps
    _.remove(roleMemory.roleCreepNames, name => !Game.creeps[name]);

    //return the remaining creeps
    return _.map(roleMemory.roleCreepNames, name =>
    {
        return Game.creeps[name]
    });
}

/**
 * returns all creeps assigned to this role that do not have an assigned goal
 * also purges the list of creeps that no longer exist
 * @param roleMemory
 */
export function getAvailableRoleCreeps(roleMemory: RoleState): Creep[]
{
    return getRoleCreeps(roleMemory).filter(creep => creep && (!creep.memory.goal));
}