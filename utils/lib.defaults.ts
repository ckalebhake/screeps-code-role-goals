//this file is responsible for creating default values for memory objects, such as RoomMemory or globalSettings

/**
 * called every tick to ensure all required objects exist
 */
export function createRequiredMemoryObjects()
{
    if(!Memory.globalSettings)
        initializeGlobalSettings();

    for (const roomName in Game.rooms)
    {
        if (!Game.rooms[roomName].memory.enabledLogTypes)
            initializeRoomMemory(roomName);

        if(!Game.rooms[roomName].memory.statistics)
            Game.rooms[roomName].memory.statistics = emptyStatistics();
    }

    if(!Memory.roles)
        initializeRoleMemory();
}

//default global settings
function initializeGlobalSettings()
{
    Memory.globalSettings = {
        enabledLogTypes: [LogType.error, LogType.warning, LogType.summary, LogType.spawning],
        defaultLogSettings: [LogType.error, LogType.warning, LogType.summary, LogType.spawning],

        autoAdjustSpawnCounts: true,
        autoConstruct: true,
        autoHarvestFlags: true,
        autoRoomSettings: true,

        desiredTerminalEnergyBalance: 30000,
        handleOldSellOrders: false,
        oldOrderThreshold: 999999,

        moveToReusePathTicks: 2,
        statisticsTrackingHistoryLength: 3000,
        spawnCountAdjustmentInterval: 3500,
    }
}

//default room memory
function initializeRoomMemory(roomName: string)
{
    Memory.rooms[roomName] = {

        repairSettings: {maxDefenseHealth: 5000, wallRepairThreshold: 0},
        enabledLogTypes: [LogType.error, LogType.summary, LogType.spawning],
        spawnSettings: {targetCreepCounts: {creeps: 0, parts: []}},

        constructionSettings: {
            forceTheseSettings: false,
            spawnExtensions: true,
            roadsBySpawnsAndExtensions: true,
            roadsByTowers: true,
            containersBySources: true,
            containersByExtractors: true,
            towers: true,
            extractors: true,
            storage: true
        },

        constructionMemory: {},

        statistics: emptyStatistics(),

        timeOfLastCreepCountAnalysis: undefined,
        spawnCountAdjustment: 0,
        remoteHarvestingInRoom: false,

        sellAtAnyPrice: [],
        buyAtAnyPrice: [],
        transferResources: [],

        lastActiveTick: Game.time
    };
}

function initializeRoleMemory()
{
    Memory.roles =
        {
            roomRoles: [],
            roomObjectRoles: [],
        }
}

function emptyStatistics(): RoomStatistics
{
    return {
        unspecializedCreepTicks: [],
        unspecializedCreepTicksSum: 0,
        unspecializedIdleCreepTicks: [],
        unspecializedIdleCreepTicksSum: 0,
        upgradesTicks: [],
        upgradesTicksSum: 0,
        upgradesThisTick: 0,
        energyPerContainerTicks: [],
        energyPerContainerTicksSum: 0
    };
}
