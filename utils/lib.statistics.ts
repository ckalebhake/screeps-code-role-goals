/**
 * tracks historical data regarding the state of the room.  Important that this is called every tick.
 * @param room
 */
import { roomLog } from "./lib.log";

export function updateRoomStatistics(room: Room): void {
    if (room.controller && room.controller.my) {
        //gather information
        const roomUnspecializedCreeps = room.find(FIND_MY_CREEPS, { filter: creep => !creep.memory.specialized });
        const roomAvailableCreeps = roomUnspecializedCreeps.filter(creep => !creep.memory.goal);
        const roomContainers = <StructureContainer[]>room.find(FIND_STRUCTURES, { filter: s => s.structureType === STRUCTURE_CONTAINER });
        const energyPerContainer = (_.sum(roomContainers, c => c.store.energy) / roomContainers.length) || 0;

        //update the arrays and sums
        room.memory.statistics.upgradesTicksSum += room.memory.statistics.upgradesThisTick;
        room.memory.statistics.upgradesTicks.push(room.memory.statistics.upgradesThisTick);
        room.memory.statistics.unspecializedCreepTicksSum += roomUnspecializedCreeps.length;
        room.memory.statistics.unspecializedCreepTicks.push(roomUnspecializedCreeps.length);
        room.memory.statistics.unspecializedIdleCreepTicksSum += roomAvailableCreeps.length;
        room.memory.statistics.unspecializedIdleCreepTicks.push(roomAvailableCreeps.length);
        room.memory.statistics.energyPerContainerTicksSum += energyPerContainer;
        room.memory.statistics.energyPerContainerTicks.push(energyPerContainer);

        //prune excess statistics entries.  splice() returns the deleted element, so we can perform the deletions and sum updates simultaneously
        if (room.memory.statistics.upgradesTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
            room.memory.statistics.upgradesTicksSum -= _.sum(room.memory.statistics.upgradesTicks.splice(0, (room.memory.statistics.upgradesTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength)));

        if (room.memory.statistics.unspecializedCreepTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
            room.memory.statistics.unspecializedCreepTicksSum -= _.sum(room.memory.statistics.unspecializedCreepTicks.splice(0, (room.memory.statistics.unspecializedCreepTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength)));

        if (room.memory.statistics.unspecializedIdleCreepTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
            room.memory.statistics.unspecializedIdleCreepTicksSum -= _.sum(room.memory.statistics.unspecializedIdleCreepTicks.splice(0, (room.memory.statistics.unspecializedIdleCreepTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength)));

        if (room.memory.statistics.energyPerContainerTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
            room.memory.statistics.energyPerContainerTicksSum -= _.sum(room.memory.statistics.energyPerContainerTicks.splice(0, (room.memory.statistics.energyPerContainerTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength)));

        //error checking
        if (Game.time % 5) {
            if (room)
                if (!(room.memory.statistics.upgradesTicksSum === _.sum(room.memory.statistics.upgradesTicks)))
                    roomLog(room.name, LogType.error, "invalid upgradesTicksSum!  (is " + room.memory.statistics.upgradesTicksSum + ", should be " + _.sum(room.memory.statistics.upgradesTicks + ")"));

            if (!(room.memory.statistics.unspecializedCreepTicksSum === _.sum(room.memory.statistics.unspecializedCreepTicks)))
                roomLog(room.name, LogType.error, "invalid unspecializedCreepTicksSum!  (is " + room.memory.statistics.unspecializedCreepTicksSum + ", should be " + _.sum(room.memory.statistics.unspecializedCreepTicks + ")"));

            if (!(room.memory.statistics.unspecializedIdleCreepTicksSum === _.sum(room.memory.statistics.unspecializedIdleCreepTicks)))
                roomLog(room.name, LogType.error, "invalid unspecializedIdleCreepTicksSum!  (is " + room.memory.statistics.unspecializedIdleCreepTicksSum + ", should be " + _.sum(room.memory.statistics.unspecializedIdleCreepTicks + ")"));

            if (!(room.memory.statistics.energyPerContainerTicksSum === _.sum(room.memory.statistics.energyPerContainerTicks)))
                roomLog(room.name, LogType.error, "invalid energyPerContainerTicksSum!  (is " + room.memory.statistics.energyPerContainerTicksSum + ", should be " + _.sum(room.memory.statistics.energyPerContainerTicks + ")"));
        }

        //reset "upgrades this tick" counter
        room.memory.statistics.upgradesThisTick = 0;
    }
    else {
        //room is now unowned.  delete statistics history to save memory.
        delete room.memory.statistics;
    }
}

/**
 * returns the averages and rounded averages for various statistics in this room
 * @param room
 */
export function roomAverages(room: Room): RoomStatisticsAverages {
    const statistics = room.memory.statistics;

    const averages = {
        creeps: statistics.unspecializedCreepTicksSum / statistics.unspecializedCreepTicks.length,
        idle: statistics.unspecializedIdleCreepTicksSum / statistics.unspecializedIdleCreepTicks.length,
        upgrades: statistics.upgradesTicksSum / statistics.upgradesTicks.length,
        energyPerContainer: statistics.energyPerContainerTicksSum / statistics.energyPerContainerTicks.length
    };

    return {
        complete: statistics.unspecializedIdleCreepTicks.length === Memory.globalSettings.statisticsTrackingHistoryLength &&
            statistics.unspecializedCreepTicks.length === Memory.globalSettings.statisticsTrackingHistoryLength &&
            statistics.upgradesTicks.length === Memory.globalSettings.statisticsTrackingHistoryLength &&
            statistics.energyPerContainerTicks.length === Memory.globalSettings.statisticsTrackingHistoryLength,

        averages: averages,
        roundedAverages: {
            creeps: Math.round(averages.creeps),
            idle: Math.round(averages.idle),
            upgrades: Math.round(averages.upgrades),
            energyPerContainer: Math.round(averages.energyPerContainer),

        }
    }
}

/**
 * logs information about room statistics
 * @param room
 */
export const logRoomStatistics = function (room: Room) {
    const statistics = room.memory.statistics;
    if (!statistics)
        return;

    const averages = roomAverages(room);

    // if (averages.complete)
    // {
    roomLog(room.name, LogType.statistics,
        ((averages.complete ? "" : "(incomplete: " + Math.min(statistics.upgradesTicks.length, statistics.unspecializedCreepTicks.length, statistics.unspecializedIdleCreepTicks.length) + "/" + Memory.globalSettings.statisticsTrackingHistoryLength + ") ")) +
        "averages: " +
        "upgrade calls: " + averages.averages.upgrades.toPrecision(2) + ", " +
        "unspecialized creeps: " + averages.averages.creeps.toPrecision(2) +
        " (" + averages.averages.idle.toPrecision(2) + " idle)" +
        ", energy per container: " + averages.roundedAverages.energyPerContainer);
    // }
    // else
    // {
    //     roomLog(room.name, LogType.statistics, "Statistic information incomplete (" +
    //         Math.min(statistics.upgradesTicks.length, statistics.unspecializedCreepTicks.length, statistics.unspecializedIdleCreepTicks.length) +
    //         "/" + Memory.globalSettings.statisticsTrackingHistoryLength + ")");
    // }
};
