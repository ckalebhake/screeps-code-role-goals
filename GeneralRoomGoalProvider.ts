import {BehaviorCollectEnergy} from "./behaviors/HarvestBehaviors";
import {BehaviorUpgrade} from "./behaviors/BehaviorUpgrade";
import {BehaviorCleanupDroppedResources} from "./behaviors/MiscBehaviors";

/**
 * this is responsible for generating general goals in a room that are unassociated with any roles
 */
export const GeneralRoomGoalProvider : GoalProvider =
{
    getGoals(targetRoom: Room)
    {
        const goals : Goal[] = [];

        //creeps in the general pool seek out energy to do other jobs with
        goals.push({
            name: "Collect energy",
            pos: new RoomPosition(25, 25, targetRoom.name),
            minimumOpenCapacity: 1,
            partsRequested: [{ part: CARRY }],
            priority: GoalPriority.standard,
            behaviorName: BehaviorCollectEnergy.name
        });

        //creeps in the general pool will work to upgrade the controller
        if (targetRoom.controller && targetRoom.controller.my)
        {
            goals.push({
                name: "Upgrade controller",
                pos: targetRoom.controller.pos,
                minimumEnergy: 10,
                partsRequested: [{ part: WORK }, { part: CARRY }],
                priority: GoalPriority.standard,
                behaviorName: BehaviorUpgrade.name
            });
        }

        const droppedResources = targetRoom.find(FIND_DROPPED_RESOURCES);
        if (droppedResources && droppedResources.length > 0)
        {
            droppedResources.forEach(droppedResource =>
            {
                if (droppedResource.resourceType === RESOURCE_ENERGY || targetRoom.storage) //only cleanup minerals if we have somewhere to put them
                {
                    goals.push(
                        {
                            name: "cleanup dropped resources",
                            pos: droppedResource.pos,
                            goalArgs: droppedResource.id,
                            partsRequested: [{part: CARRY}],
                            priority: GoalPriority.important,
                            behaviorName: BehaviorCleanupDroppedResources.name,
                            minimumOpenCapacity: 10,
                            prefersOpenCapacity: droppedResource.amount,
                        }
                    )
                }
            })
        }

        return goals;
    }
};