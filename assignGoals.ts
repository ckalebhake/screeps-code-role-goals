/**
 * assigns the given goals to the given creeps
 * @param creeps
 * @param goals
 */
import {globalLog} from "./utils/lib.log";
import {isCreepPreferred, isCreepQualified} from "./utils/lib.creep";
import {getAvailableRoleCreeps} from "./utils/lib.role";

export function assignGoals(creeps : Creep[], goals: Goal[])
{
    goals = _.sortBy(goals, "priority").reverse();

    //bail early if assignments are impossible
    if (creeps.length === 0 || goals.length === 0)
        return;

    globalLog(LogType.debugGoalAssignment, "assigning " + goals.length + " goals to " + creeps.length + " creeps. (" + JSON.stringify(_.countBy(goals, "priority")) + ")");

    //make a list of ALL valid combinations of creep and goal
    let validCombinations : {creep: Creep, goal: Goal, distance: number, preferred: boolean}[] = [];
    creeps.forEach(creep => {
        goals.forEach(goal => {
            if (isCreepQualified(creep, goal))
            {
                validCombinations.push({
                    creep: creep,
                    goal: goal,
                    distance: creep.pos.getRangeTo(goal.pos),
                    preferred: isCreepPreferred(creep, goal)
                })
            }
        });
    });

    //sort them by lowest distance
    validCombinations = _.sortBy(validCombinations, "distance");

    globalLog(LogType.debugGoalAssignment, validCombinations.length + " valid combinations found.");

    let assignmentCount = 0;
    //main assignment loop
    while (validCombinations.length > 0)
    {
        //all goals with the same priority as the highest priority goal
        //by looping through just this subset, we always respect priority before anything else
        const topPriority = _.max(validCombinations, c => c.goal.priority).goal.priority;
        let topPriorityCombinations = _.remove(validCombinations, combo => combo.goal.priority === topPriority);

        while (topPriorityCombinations.length > 0)
        {
            //first look for a preferred combination.  If that does not exist, just take the first one
            const assignment = _.find(topPriorityCombinations, tpc => tpc.preferred) || _.first(topPriorityCombinations);

            //assign it
            assignment.creep.memory.goal = assignment.goal;
            assignmentCount++;
            globalLog(LogType.debugGoalAssignment, "assignment: " + assignment.creep.name + "=>" + assignment.goal.name
                + " (distance: " + assignment.distance + ", preferred: " + assignment.preferred + ")");

            //remove it, and all conflicting assignments, from both lists
            _.remove(topPriorityCombinations, tpc => tpc.creep === assignment.creep || tpc.goal === assignment.goal);
            _.remove(validCombinations, tpc => tpc.creep === assignment.creep || tpc.goal === assignment.goal);
        }
    }

    globalLog(LogType.debugGoalAssignment, "assigned a total of " + assignmentCount + " goals.");
}

/**
 * helper function to call assinGoals for all creeps and goals managed by the given role
 * @param role
 * @param roleState
 */
export function assignRoleGoals(role : Role, roleState: RoleState)
{
    const creeps : Creep[] = getAvailableRoleCreeps(roleState);
    const goals : Goal[] = role.getGoals(roleState);
    assignGoals(creeps, goals);
}