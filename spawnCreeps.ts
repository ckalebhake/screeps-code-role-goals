import { globalLog } from "./utils/lib.log";
import { costOfConfig, creepConfigurations } from "./utils/lib.creep";
import { BehaviorGoToRoom } from "./behaviors/MiscBehaviors";
import { getRoleByName } from "./roles/GetRoleByName";

/**
 * handles spawning new creeps
 * @param requests to spawn a creep
 */
export function spawnCreeps(requests: CreepRequest[]) {
    //skip spawning if nothing made a request
    if (!requests || requests.length === 0)
        return;

    //find which spawns are available for spawning creeps
    const availableSpawns = _.filter(Game.spawns, (spawn: StructureSpawn) => {
        return !spawn.spawning &&
            spawn.room.energyAvailable >= 50 &&
            !spawn.memory.disabled;
    });

    globalLog(LogType.summary,
        "Creep Requests: " + requests.length +
        " (" + JSON.stringify(_.countBy(requests, rq => (rq.requestedByRole || { roleName: null }).roleName)) + ")" +
        ", Spawns: " + _.size(availableSpawns) +
        "/" + _.size(Game.spawns));

    if (_.size(availableSpawns) === 0)
        return;

    //sort the requests, with the highest priority first
    requests = _.sortBy(requests, "priority").reverse();

    requests.forEach(request => {
        globalLog(LogType.debugSpawning, "==========");
        globalLog(LogType.debugSpawning, "request: " + JSON.stringify(request));

        //just a little error checking
        if (request.state !== CreepRequestState.OPEN) {
            globalLog(LogType.warning, "a creep request of state " + request.state + " was passed to spawnCreeps!");
            return;
        }

        //find body configurations that can satisfy the request
        const validConfigurations = _.filter(creepConfigurations, creepConfig => {
            if (request.minimumTier && creepConfig.tier < request.minimumTier)
                return false;

            if (request.requestedRoles)
                if (request.requestedRoles.some(role => { return !creepConfig.supportedRoles.includes(role); }))
                    return false;

            if (request.requestedParts) {
                const configPartCounts = _.countBy(creepConfig.body);
                if (request.requestedParts.some(prq => { return configPartCounts[prq.part] < prq.count }))
                    return false;
            }

            return true;
        });

        if (validConfigurations.length === 0) {
            globalLog(LogType.error, "No creep configuration supports the request " + JSON.stringify(request));
            return;
        }

        const preferredConfigurations = request.preferredTier ? validConfigurations.filter(config => { return config.tier >= request.preferredTier! }) : null;
        globalLog(LogType.debugSpawning,
            validConfigurations.length + " creep configurations to choose from" +
            (preferredConfigurations ? " (" + preferredConfigurations!.length + " preferred)" : ""));

        //calculate spawn costs
        const minCost = Math.min(..._.map(validConfigurations, config => { return costOfConfig(config) }));
        const preferredCost = preferredConfigurations ? Math.min(..._.map(preferredConfigurations, config => { return costOfConfig(config) })) : null;

        //find spawns that can handle this specific request
        const qualifiedSpawns = _.filter(availableSpawns, (spawn: StructureSpawn) => {
            if (request.spawnFromRoom)
                if (spawn.room.name !== request.spawnFromRoom)
                    return false;

            if (spawn.room.energyAvailable < minCost)
                return false;

            return spawn.pos.inRangeTo(request.requestPos, 50);
        });

        const preferredSpawns = preferredCost ? qualifiedSpawns.filter(spawn => { return spawn.room.energyAvailable >= preferredCost }) : null;

        globalLog(LogType.debugSpawning,
            qualifiedSpawns.length + " spawns to choose from" +
            (preferredSpawns ? " (" + preferredSpawns!.length + " preferred)" : ""));

        if (qualifiedSpawns.length === 0)
            return;

        let targetSpawn: StructureSpawn | null = null;
        if (preferredSpawns)
            targetSpawn = request.requestPos.findClosestByPath(preferredSpawns);

        if (!targetSpawn)
            targetSpawn = request.requestPos.findClosestByPath(qualifiedSpawns);

        if (!targetSpawn) {
            globalLog(LogType.debugSpawning, "could not find a path to a qualified spawn");
            return;
        }

        globalLog(LogType.debugSpawning, "selected spawn " + targetSpawn.name + " at " + JSON.stringify(targetSpawn.pos));

        let targetConfiguration: CreepConfiguration | null = null;
        if (preferredCost && targetSpawn.room.energyAvailable >= preferredCost) {
            targetConfiguration = _.find(_.sortBy(preferredConfigurations!, "tier").reverse(), config => {
                //highest tier preferred configuration that we can afford
                return costOfConfig(config) <= targetSpawn!.room.energyAvailable;
            }) || null;
        }
        if (!targetConfiguration) {
            //highest tier valid configuration that we can afford
            targetConfiguration = _.find(_.sortBy(validConfigurations, "tier").reverse(), config => {
                return costOfConfig(config) <= targetSpawn!.room.energyAvailable;
            }) || null;
        }

        if (!targetConfiguration) {
            globalLog(LogType.warning, "could not choose a creep configuration!  This really should not happen by this point in the algorithm.");
            return;
        }

        globalLog(LogType.debugSpawning, "selected configuration " + targetConfiguration.name + " (tier " + targetConfiguration.tier + ")");

        const creepName = Math.random().toString(36).slice(2); //randomly generate creep name
        const spawnResult = targetSpawn.spawnCreep(targetConfiguration.body, creepName, //body from the configuration
            {
                memory: {
                    supportedRoles: targetConfiguration.supportedRoles,
                    preferredRoles: targetConfiguration.preferredRoles,
                    specialized: request.specialized,
                    spawnedFromRoom: targetSpawn.room.name,

                    goal: request.requestedForGoal ? request.requestedForGoal :             //if spawned for a specific goal, the new creep will start with that goal
                        request.requestPos.roomName === targetSpawn.room.name ? undefined : //not spawned for a specific goal, but was spawned for this room
                            {
                                //not spawned for a specific goal, but we were spawned for a specific room, and it wasn't this one
                                //therefore, the creep should move to the room it was intended for
                                name: "new creep moving to room " + request.requestPos.roomName,
                                partsRequested: [],
                                priority: GoalPriority.important,
                                behaviorName: BehaviorGoToRoom.name,
                                pos: request.requestPos,
                                goalArgs: request.requestPos.roomName
                            },

                    roleName: request.requestedByRole ? request.requestedByRole.roleName : undefined
                }
            }
        );
        switch (spawnResult) {
            case OK:
                request.state = CreepRequestState.SPAWNING;
                request.creepName = creepName;
                request.spawnName = targetSpawn.name;

                if (request.requestedByRole) {
                    const requestingRole = getRoleByName(request.requestedByRole.roleName);
                    if (requestingRole)
                        requestingRole.onCreepSpawning(creepName, request.requestedByRole);
                }
                break;
            case ERR_NOT_ENOUGH_ENERGY:
                globalLog(LogType.warning, "not enough energy to spawn creep!  Something is probably wrong in spawnCreeps.ts.");
                break;
            default:
                globalLog(LogType.error, "Unhandled spawnCreep() result: " + spawnResult);
        }
    });
}
