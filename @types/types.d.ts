// type shim for nodejs' `require()` syntax
// for stricter node.js typings, remove this and install `@types/node`
// declare const require: (module: string) => any;

// add your custom typings here
/**
 * defines how a creep will carry out the associated goal
 */
interface Behavior
{
    name: string,

    run(creep: Creep, goalArgs?: any): void;
}

/**
 * represents a request for some number of a specific body part
 */
interface PartRequest
{
    part: BodyPartConstant, //desired part
    min?: number,           //minimum required (default: 1)
    preferred?: number      //preferred amount

    //maximum of this part to be put on the goal.
    // Note that this is a "soft cap", meaning an assignment can put it over this number,
    // but no new assignments will be made if the cap has already been reached
    max?: number
}

/**
 * used for aggregate counting of creeps and their body parts
 */
interface CreepCounts
{
    creeps: number
    parts: CreepPartCount[];
}

interface CreepPartCount
{
    part: BodyPartConstant,
    count: number
}

/**
 * represents x and y offsets from some position
 */
interface PositionOffset
{
    x: number,
    y: number
}

interface SpawnMemory
{
    disabled?: boolean,          //if true, this spawn is always considered unavailable for spawning
    creepRequest?: CreepRequest  //the request this spawn is currently serving
}

interface RemoteSpawnRequest
{
    requestingFlagName: string,          //name of the flag desiring help
}

//add signature for a JS function that, oddly, TS doesn't recognize on its own
interface Math
{
    sign(x: number): number
}

//represents a predefined creep configuration
interface CreepConfiguration
{
    name: string,                           //just for logging
    tier: number,                           //rough indicator of quality; higher tiers are better
    body: BodyPartConstant[],               //the actual body parts
    supportedRoles: CreepRoleConstant[],    //which roles the creep CAN do
    preferredRoles?: CreepRoleConstant[],   //which roles the creep WANTS to do
}

type SuppliableStructure = StructureSpawn | StructureExtension | StructureTower | StructureLab | StructureNuker;

