/**
 * how roles are stored in memory
 */
interface RoleMemory
{
    roomRoles:       RoleStateRoom[];
    roomObjectRoles: RoleStateRoomObject[];
}

/**
 * GoalProviders are responsible for providing a set of Goals.
 * Typically they are associated with some sort of object, and examine the object to determine whether or not
 * action should be taken.
 */
interface GoalProvider
{
    /**
     * examines the given object and returns an array of Goals based on that object.
     * Note that this array may be empty.
     */
    getGoals(target: any): Goal[];
}

/**
 * an object meant to provide creep requests to the spawning logic
 */
interface CreepRequestProvider
{
    getCreepRequests(target: any): CreepRequest[];
}

/**
 * Not to be confused with behaviors, a Role is a means of keeping a set of creeps dedicated to some specific task
 * It maintains a list of creeps which are dedicated to perform one or more specific goals
 */
interface Role extends CreepRequestProvider, GoalProvider
{
    name: string,

    //these should be overridden by subtypes to have more meaningful arguments
    getState(target: any): RoleState;
    getOrCreateState(target: any): RoleState;
    onCreepSpawning(creepName: string, roleState: RoleState): void;
}

//extensions of Role that clarify argument typing
interface RoleForRoom extends Role
{
    getState(roomName: string): RoleState;
    getOrCreateState(roomName: string): RoleState;
    onCreepSpawning(creepName: string, roleState: RoleStateRoom): void;
    getGoals(roleState: RoleStateRoom): Goal[];
    getCreepRequests(roleState: RoleStateRoom): CreepRequest[];
}

interface RoleForRoomObject extends Role
{
    getState(target: {roomName: string, objectId: string}): RoleState;
    getOrCreateState(target: {roomName: string, objectId: string}): RoleState;
    onCreepSpawning(creepName: string, roleState: RoleStateRoomObject): void;
    getGoals(roleState: RoleStateRoomObject): Goal[];
    getCreepRequests(roleState: RoleStateRoomObject): CreepRequest[];
}

interface RoleState
{
    roleName: string;
    target: any;
    roleCreepNames: string[];
    roleSpecific?: any;
}

interface RoleStateRoom extends RoleState
{
    target: string;
}

interface RoleStateRoomObject extends RoleState
{
    target:
    {
        roomName: string;
        objectId: string;
    }
}