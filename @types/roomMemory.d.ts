/**
 * defines how room memory should be structured
 */
interface RoomMemory
{
    //what kinds of logging should be done
    enabledLogTypes: LogType[],

    //various bits of room settings
    repairSettings: RoomSettingsForRepairs,
    spawnSettings: RoomSettingsForSpawning
    constructionSettings: RoomSettingsForConstruction,

    //stores room construction metadata
    constructionMemory:
        {
            nextSpawnExtensionOffset?: PositionOffset,
            SpawnExtensionBuildDirection?: TOP | RIGHT | BOTTOM | LEFT,
        },

    //statistics
    statistics: RoomStatistics,

    //automation metadata
    timeOfLastCreepCountAnalysis?: number, //so autoAdjustSpawnCounts knows when to run
    spawnCountAdjustment?: number,         //adjustment maintained by autoAdjustSpawnCounts
    remoteHarvestingInRoom?: boolean,      //if true, some forms of high level automation apply even if the room is unowned

    //trading
    sellAtAnyPrice?: { resource: ResourceConstant, amount: number }[],
    buyAtAnyPrice?: { resource: ResourceConstant, amount: number }[],
    transferResources?: { resource: ResourceConstant, amount: number, destination: string }[],

    //activity tracking
    lastActiveTick: number,
}