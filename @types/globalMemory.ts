// how global memory is structured
interface Memory
{
    globalSettings: GlobalSettings,
    globalAutomationMemory?: GlobalAutomationMemory,
    creeps: { [index: string]: CreepMemory },
    flags: { [index: string]: FlagMemory },
    rooms: { [index: string]: RoomMemory },
    spawns: { [index: string]: SpawnMemory },
    roles: RoleMemory,
}

interface GlobalAutomationMemory
{
    handleOldOrders?: { orderId: string, lastAdjustedTime: number }[]
}