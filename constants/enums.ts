const enum CreepRequestPriority
{
    MINIMUM = 1,
    LOW = 10,
    MEDIUM = 20,
    HIGH = 30,
    CRITICAL = 1000,
}

const enum CreepRequestState
{
    OPEN = "OPEN",
    SPAWNING = "SPAWNING",
    CANCELLED = "CANCELLED",
    FULFILLED = "FULFILLED",
}

const enum CreepRoleConstant
{
    UPGRADER = "UPGRADER",
    GRUNT = "GRUNT",
    HARVESTER = "HARVESTER",
    CARRIER = "CARRIER",
}

const enum LogType
{
    info                  = "info",
    debugGoalAssignment   = "debugGoalAssignment",
    debugSpawning         = "debugSpawning",
    debugBehavior         = "debugBehavior",
    debugRole             = "debugRole",
    error                 = "error",
    market                = "market",
    spawning              = "spawning",
    summary               = "summary",
    statistics            = "statistics",
    trace                 = "trace",
    warning               = "warning",
}

const enum GoalPriority
{
    critical = 999,
    important = 100,
    standard = 50,
    busywork = 1
}