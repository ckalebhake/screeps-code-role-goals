/**
 * this role maintains a SINGLE creep to keep upgrading the room controller.
 */
import {BehaviorCollectEnergy} from "../behaviors/HarvestBehaviors";
import {BehaviorUpgrade} from "../behaviors/BehaviorUpgrade";
import {getRoleCreeps} from "../utils/lib.role";

export const RoomRoleMaintainController: RoleForRoom =
{
    name: "RoomRoleMaintainController",
    getCreepRequests: function (roleState: RoleStateRoom): CreepRequest[] {
        //skip if we do not have vision on the room
        if (!Game.rooms[roleState.target])
            return [];

        const controller: StructureController | undefined = Game.rooms[roleState.target].controller;

        //skip if the room is not ours
        if (!controller || !controller.my)
            return [];

        if (getRoleCreeps(roleState).length > 0) {
            return [];
        }
        else {
            return [
                {
                    requestedRoles: [CreepRoleConstant.UPGRADER],
                    requestPos: controller.pos,
                    state: CreepRequestState.OPEN,

                    //critical priority if the room is about to downgrade.  Medium priority otherwise
                    priority: (controller.ticksToDowngrade <= 4000) ? CreepRequestPriority.CRITICAL : CreepRequestPriority.MEDIUM,

                    specialized: true,

                    requestedByRole: roleState
                }
            ];
        }
    },

    getGoals: function (roleState: RoleStateRoom): Goal[] {
        //skip if we do not have vision on the room
        const room: Room | undefined = Game.rooms[roleState.target];
        if (!room || !room.controller)
            return [];

        return [
            {
                name: "Upgrade controller",
                pos: room.controller.pos,
                minimumEnergy: 10,
                partsRequested: [{ part: WORK }, { part: CARRY }],
                priority: (room.controller.ticksToDowngrade < 4000) ? GoalPriority.critical : GoalPriority.important,
                behaviorName: BehaviorUpgrade.name
            },
            {
                name: "Collect energy",
                pos: room.controller.pos,
                minimumOpenCapacity: 1,
                partsRequested: [{ part: CARRY }],
                priority: GoalPriority.standard,
                behaviorName: BehaviorCollectEnergy.name
            }
        ];
    },

    //called when a creep is spawned for this role.  Adds it to the roleCreeps.
    onCreepSpawning: function (creepName: string, roleState: RoleStateRoom)
    {
        roleState.roleCreepNames.push(creepName);
    },

    //returns the memory for this role in the given room
    getState: function (roomName : string): RoleStateRoom
    {
        return _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;
    },

    getOrCreateState: function (roomName : string): RoleStateRoom
    {
        let state: RoleStateRoom = _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;

        if(!state)
        {
            state =
                {
                    roleCreepNames: [],
                    roleName: this.name,
                    target: roomName,
                };

            Memory.roles.roomRoles.push(state);
        }

        return state;
    }
};
