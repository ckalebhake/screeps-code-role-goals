import {getRoleCreeps} from "../utils/lib.role";
import {BehaviorCollectEnergy} from "../behaviors/HarvestBehaviors";
import {countCreepsFromArray} from "../utils/lib.creep";
import {BehaviorRepair} from "../behaviors/MiscBehaviors";
import {roomLog} from "../utils/lib.log";
import {structuresNeedingRepair} from "../utils/lib.room";

export const RoomRoleRepair : RoleForRoom =
{
    name: "RoomRoleRepair",

    getCreepRequests: function (roleState: RoleStateRoom): CreepRequest[]
    {
        //clear out creeps that no longer exist and get the actual creep objects
        const creeps = getRoleCreeps(roleState);

        //count up what we have
        const creepCounts = countCreepsFromArray(creeps);

        //determine how much repair work needs to be done
        let repairsToDo = 0;
        const room: Room | undefined = Game.rooms[roleState.target];
        if (!room || !room.controller)
            return [];

        const damagedStructures = structuresNeedingRepair(room);
        damagedStructures.forEach(ds => {
            let hitsMax = ds.hitsMax;
            if(ds.structureType === STRUCTURE_WALL || ds.structureType === STRUCTURE_RAMPART)
                hitsMax = Math.min(hitsMax, room.memory.repairSettings.maxDefenseHealth);
            repairsToDo += (hitsMax - ds.hits);
        });

        //determine how many WORK parts we want to handle the repairs
        const desiredWorkParts = Math.min(1, Math.floor(repairsToDo / 30000));
        const currentWorkParts = (creepCounts.parts.find(p => p.part === WORK) || {count: 0}).count;

        roomLog(roleState.target, LogType.debugRole, repairsToDo + " points of repairs needed across " +
            damagedStructures.length + " structures by " + creeps.length + " creeps with " +
            currentWorkParts + "/" + desiredWorkParts + " work parts");

        //if we still need more work parts, request more creeps
        if ( currentWorkParts >= desiredWorkParts )
        {
            return [];
        }
        else
        {
            return [{
                requestedByRole: roleState,
                requestPos: new RoomPosition(25, 25, room.name),
                priority: CreepRequestPriority.MEDIUM,
                state: CreepRequestState.OPEN,
                specialized: true,
                requestedRoles: [CreepRoleConstant.GRUNT],
            }];
        }
    },

    getGoals: function (roleState: RoleStateRoom): Goal[]
    {
        //bail early if no creeps
        if(roleState.roleCreepNames.length === 0)
            return [];

        const targetRoom = Game.rooms[roleState.target];
        if(!targetRoom || !targetRoom.controller || !targetRoom.controller.my)
            return [];

        const goals: Goal[] = [];
        structuresNeedingRepair(targetRoom).forEach(structure => {
            goals.push({
                name: "Repair " + structure.id,
                partsRequested: [{part: "work", min: 1}],
                priority: (structure.hits < (structure.hitsMax * 0.1))? GoalPriority.important : GoalPriority.standard,
                behaviorName: BehaviorRepair.name,
                goalArgs: structure.id,
                pos: structure.pos,
                minimumEnergy: 10,
                prefersEnergy: structure.hitsMax - structure.hits,
            });
        });

        goals.push({
            name: "Collect energy",
            pos: targetRoom.controller!.pos,
            minimumOpenCapacity: 1,
            partsRequested: [{ part: CARRY }],
            priority: GoalPriority.standard,
            behaviorName: BehaviorCollectEnergy.name
        });

        //if all repairs are done, we can free creeps from this role
        if(goals.length === 1)
        {
            roomLog(targetRoom.name, LogType.info, "All repairs complete.  Freeing creeps from the repairs role.");

            getRoleCreeps(roleState).forEach(creep => {
                creep.memory.specialized = false;
                delete creep.memory.roleName;
            });

            roleState.roleCreepNames = [];
        }

        return goals;
    },

    //returns the memory for this role in the given room
    getState: function (roomName : string): RoleStateRoom
    {
        return _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;
    },

    getOrCreateState: function (roomName : string): RoleStateRoom
    {
        let state: RoleStateRoom = _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;

        if(!state)
        {
            state =
                {
                    roleCreepNames: [],
                    roleName: this.name,
                    target: roomName,
                };

            Memory.roles.roomRoles.push(state);
        }

        return state;
    },

    onCreepSpawning: function (creepName: string, roleState: RoleStateRoom)
    {
        roleState.roleCreepNames.push(creepName);
    }
};