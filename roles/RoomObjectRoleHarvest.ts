/**
 * handles maintaining creeps to harvest from energy sources in the room
 */
import {countCreepsFromArray} from "../utils/lib.creep";
import {BehaviorHarvestEnergy} from "../behaviors/HarvestBehaviors";
import {getRoleCreeps} from "../utils/lib.role";

export const RoomObjectRoleHarvest: RoleForRoomObject =
{
    name: "RoomObjectRoleHarvest",
    getCreepRequests: function(roleState: RoleStateRoomObject): CreepRequest[]
    {
        //clear out creeps that no longer exist and get the actual creep objects
        const creeps = getRoleCreeps(roleState);

        //count up what we have
        const creepCounts = countCreepsFromArray(creeps);

        //determine how much walkable space is around the target source
        const targetSource = Game.getObjectById(roleState.target.objectId) as Source;
        let walkableSpaces = 0;
        for (let x = -1; x <= 1; x++)
            for (let y = -1; y <=1; y++)
                if(x !== 0 || y !== 0)
                    if(!Game.map.getRoomTerrain(roleState.target.roomName).get(targetSource.pos.x + x, targetSource.pos.y + y))
                        walkableSpaces++;

        //determine how many WORK parts we need to drain the source before it refills
        const desiredWorkParts = (targetSource.energyCapacity / ENERGY_REGEN_TIME);
        const currentWorkParts = (creepCounts.parts.find(p => p.part === WORK) || {count: 0}).count;

        //if we still need more work parts and we have not saturated the source, request more creeps
        if ( (creepCounts.creeps >= walkableSpaces) || (currentWorkParts >= desiredWorkParts) )
        {
            return [];
        }
        else
        {
            return [{
                requestedByRole: roleState,
                requestPos: targetSource.pos,
                priority: (creeps.length === 0) ? CreepRequestPriority.CRITICAL : CreepRequestPriority.HIGH,
                state: CreepRequestState.OPEN,
                specialized: true,
                requestedRoles: [CreepRoleConstant.HARVESTER],
                preferredTier: Math.ceil((desiredWorkParts - currentWorkParts) / 2),
            }];
        }
    },

    getGoals: function(roleState: RoleStateRoomObject): Goal[]
    {
        return [{
            name: "harvest source " + roleState.target.objectId,
            partsRequested: [{part: "work", min: 1}],
            priority: CreepRequestPriority.CRITICAL,
            behaviorName: BehaviorHarvestEnergy.name,
            pos: (Game.getObjectById(roleState.target.objectId) as Source)!.pos,
            goalArgs: roleState.target.objectId,
        }];
    },

    onCreepSpawning: function (creepName: string, roleState: RoleStateRoomObject)
    {
        roleState.roleCreepNames.push(creepName);
    },

    //returns the memory for this role in the given room
    getState: function (target: {roomName : string, objectId : string}): RoleStateRoomObject
    {
        return _.find(Memory.roles.roomObjectRoles, roleState => {
            return roleState.roleName === this.name &&
                   roleState.target.roomName === target.roomName &&
                   roleState.target.objectId === target.objectId;
        }) as RoleStateRoomObject;
    },

    getOrCreateState: function (target: {roomName : string, objectId : string}): RoleStateRoomObject
    {
        let state: RoleStateRoomObject = _.find(Memory.roles.roomObjectRoles, roleState => {
            return roleState.roleName === this.name &&
                _.eq(roleState.target, target);
        }) as RoleStateRoomObject;

        if(!state)
        {
            state =
                {
                    roleCreepNames: [],
                    roleName: this.name,
                    target: target,
                };

            Memory.roles.roomObjectRoles.push(state);
        }

        return state;
    }
};