import {RoomObjectRoleHarvest} from "./RoomObjectRoleHarvest";
import {RoomRoleMaintainController} from "./RoomRoleMaintainController";
import {RoomRoleSupply} from "./RoomRoleSupply";
import {RoomRoleConstruct} from "./RoomRoleConstruct";
import {RoomRoleRepair} from "./RoomRoleRepair";

export const getRoleByName = function(roleName: string): Role | null
{
    switch(roleName)
    {
        case "RoomObjectRoleHarvest":      return RoomObjectRoleHarvest;
        case "RoomRoleMaintainController": return RoomRoleMaintainController;
        case "RoomRoleSupply":             return RoomRoleSupply;
        case "RoomRoleConstruct":          return RoomRoleConstruct;
        case "RoomRoleRepair":             return RoomRoleRepair;
        default: return null;
    }
};