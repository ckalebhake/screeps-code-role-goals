import {RoomRoleMaintainController} from "./RoomRoleMaintainController";
import {RoomObjectRoleHarvest} from "./RoomObjectRoleHarvest";
import {RoomRoleSupply} from "./RoomRoleSupply";
import {RoomRoleConstruct} from "./RoomRoleConstruct";
import {RoomRoleRepair} from "./RoomRoleRepair";

export function createBasicRoomRolesIfAbsent(room: Room): void
{
    //only create for owned rooms
    if (!room.controller || !room.controller.my)
        return;

    //maintain the controller
    RoomRoleMaintainController.getOrCreateState(room.name);

    //harvest from all sources
    room.find(FIND_SOURCES).forEach(source => RoomObjectRoleHarvest.getOrCreateState({
        roomName: room.name,
        objectId: source.id
    }));

    //supply structures in the room
    RoomRoleSupply.getOrCreateState(room.name);

    //perform construction in the room
    RoomRoleConstruct.getOrCreateState(room.name);

    //keep structures repaired in the room
    RoomRoleRepair.getOrCreateState(room.name);
}