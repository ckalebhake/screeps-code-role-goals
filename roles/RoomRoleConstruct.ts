import {getRoleCreeps} from "../utils/lib.role";
import {BehaviorCollectEnergy} from "../behaviors/HarvestBehaviors";
import {countCreepsFromArray} from "../utils/lib.creep";
import {BehaviorConstruct} from "../behaviors/MiscBehaviors";
import {roomLog} from "../utils/lib.log";

export const RoomRoleConstruct : RoleForRoom =
{
    name: "RoomRoleConstruct",

    getCreepRequests: function (roleState: RoleStateRoom): CreepRequest[]
    {
        //clear out creeps that no longer exist and get the actual creep objects
        const creeps = getRoleCreeps(roleState);

        //count up what we have
        const creepCounts = countCreepsFromArray(creeps);

        //determine how much construction needs to be done
        let constructionToDo = 0;
        const room: Room | undefined = Game.rooms[roleState.target];
        if (!room || !room.controller)
            return [];

        const constructionSites = room.find(FIND_MY_CONSTRUCTION_SITES);
        constructionSites.forEach(cs => constructionToDo += (cs.progressTotal - cs.progress));

        //determine how many WORK parts we want to handle the construction
        const desiredWorkParts = Math.ceil(constructionToDo / 3000);
        const currentWorkParts = (creepCounts.parts.find(p => p.part === WORK) || {count: 0}).count;

        roomLog(roleState.target, LogType.debugRole, constructionToDo + " points of construction needed across " +
            constructionSites.length + " construction sites by " + creeps.length + " creeps with " +
            currentWorkParts + "/" + desiredWorkParts + " work parts");

        //if we still need more work parts, request more creeps
        if ( currentWorkParts >= desiredWorkParts )
        {
            return [];
        }
        else
        {
            return [{
                requestedByRole: roleState,
                requestPos: new RoomPosition(25, 25, room.name),
                priority: CreepRequestPriority.MEDIUM,
                state: CreepRequestState.OPEN,
                specialized: true,
                requestedRoles: [CreepRoleConstant.GRUNT],
            }];
        }
    },

    getGoals: function (roleState: RoleStateRoom): Goal[]
    {
        //bail early if no creeps
        if(roleState.roleCreepNames.length === 0)
            return [];

        const targetRoom = Game.rooms[roleState.target];
        if(!targetRoom || !targetRoom.controller || !targetRoom.controller.my)
            return [];

        const goals: Goal[] = [];
        targetRoom.find(FIND_MY_CONSTRUCTION_SITES).forEach(site => {
            goals.push({
                name: "Construct " + site.id,
                partsRequested: [{part: "work", min: 1}],
                priority: GoalPriority.standard,
                behaviorName: BehaviorConstruct.name,
                goalArgs: site.id,
                pos: site.pos,
                minimumEnergy: 10,
                prefersEnergy: site.progressTotal - site.progress,
            });
        });

        goals.push({
            name: "Collect energy",
            pos: targetRoom.controller!.pos,
            minimumOpenCapacity: 1,
            partsRequested: [{ part: CARRY }],
            priority: GoalPriority.standard,
            behaviorName: BehaviorCollectEnergy.name
        });

        //if all construction is done, we can free creeps from this role
        if(goals.length === 1)
        {
            roomLog(targetRoom.name, LogType.info, "All construction complete.  Freeing creeps from the construction role.");

            getRoleCreeps(roleState).forEach(creep => {
                creep.memory.specialized = false;
                delete creep.memory.roleName;
            });

            roleState.roleCreepNames = [];
        }

        return goals;
    },

    //returns the memory for this role in the given room
    getState: function (roomName : string): RoleStateRoom
    {
        return _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;
    },

    getOrCreateState: function (roomName : string): RoleStateRoom
    {
        let state: RoleStateRoom = _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;

        if(!state)
        {
            state =
                {
                    roleCreepNames: [],
                    roleName: this.name,
                    target: roomName,
                };

            Memory.roles.roomRoles.push(state);
        }

        return state;
    },

    onCreepSpawning: function (creepName: string, roleState: RoleStateRoom)
    {
        roleState.roleCreepNames.push(creepName);
    }
};