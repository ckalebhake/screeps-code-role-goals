import { getRoleCreeps } from "../utils/lib.role";
import { roomStructuresNeedingSupply, roomSuppliableStructures } from "../utils/lib.room";
import { BehaviorSupplyStructure } from "../behaviors/SupplyBehaviors";
import { BehaviorCollectEnergy } from "../behaviors/HarvestBehaviors";
import { roomLog } from "../utils/lib.log";

export const RoomRoleSupply: RoleForRoom =
{
    name: "RoomRoleSupply",

    /**
     * determines how many creeps are needed to supply structures in the room, and keeps that many handy
     * @param roleState
     */
    getCreepRequests: function (roleState: RoleStateRoom): CreepRequest[] {
        //make sure we can see the room
        const room = Game.rooms[roleState.target];
        if (!room)
            return [];

        //gather up room state
        const creeps = getRoleCreeps(roleState);
        const creepCarryParts = _.sum(creeps, creep => {
            return creep.body.filter(part => part.type === "carry" && part.hits > 0).length;
        });
        const suppliableStructures = roomSuppliableStructures(room);
        const structuresNeedingEnergy = roomStructuresNeedingSupply(room);
        const energyToDistribute = _.sum(structuresNeedingEnergy, structure => structure.energyCapacity - structure.energy);
        const roomEnergyCapacity = _.sum(suppliableStructures, structure => structure.energyCapacity);

        //make decisions about how many creeps we want doing this
        const currentDesiredCreeps = Math.ceil(structuresNeedingEnergy.length / 10);
        const minimumDesiredCreeps = Math.ceil(suppliableStructures.length / 20);
        const desiredSupplyCreeps = Math.min(currentDesiredCreeps, minimumDesiredCreeps);

        const currentDesiredCarryParts = Math.ceil(energyToDistribute / 100);
        const minimumDesiredCarryParts = Math.ceil(roomEnergyCapacity / 300);
        const desiredCarryParts = Math.min(currentDesiredCreeps, minimumDesiredCreeps);

        roomLog(roleState.target, LogType.debugRole, energyToDistribute + "/" + roomEnergyCapacity + " energy to distribute to " +
            structuresNeedingEnergy.length + "/" + suppliableStructures.length + " structures by " +
            creeps.length + "/" + desiredSupplyCreeps + " creeps with " +
            creepCarryParts + "/" + desiredCarryParts + " carry parts");

        if (creeps.length < desiredSupplyCreeps)
            return [{
                requestPos: new RoomPosition(25, 25, roleState.target),
                priority: CreepRequestPriority.HIGH,
                state: CreepRequestState.OPEN,
                specialized: true,
                requestedRoles: [CreepRoleConstant.CARRIER],
                requestedByRole: roleState
            }];
        else
            return [];
    },

    getGoals: function (roleState: RoleStateRoom): Goal[] {
        const targetRoom = Game.rooms[roleState.target];
        if (!targetRoom || !targetRoom.controller || !targetRoom.controller.my)
            return [];

        const goals: Goal[] = [];
        roomStructuresNeedingSupply(targetRoom).forEach(structure => {
            goals.push({
                name: "Supply " + structure.id,
                partsRequested: [{ part: "carry", min: 1 }],
                priority: GoalPriority.important,
                behaviorName: BehaviorSupplyStructure.name,
                goalArgs: structure.id,
                pos: structure.pos,
                minimumEnergy: 10,
                prefersEnergy: structure.energyCapacity - structure.energy,
            });
        });

        goals.push({
            name: "Collect energy",
            pos: targetRoom.controller!.pos,
            minimumOpenCapacity: 1,
            partsRequested: [{ part: CARRY }],
            priority: GoalPriority.standard,
            behaviorName: BehaviorCollectEnergy.name
        });

        return goals;
    },

    //returns the memory for this role in the given room
    getState: function (roomName: string): RoleStateRoom {
        return _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;
    },

    getOrCreateState: function (roomName: string): RoleStateRoom {
        let state: RoleStateRoom = _.find(Memory.roles.roomRoles, roleState => {
            return roleState.roleName === this.name && roleState.target === roomName
        }) as RoleStateRoom;

        if (!state) {
            state =
                {
                    roleCreepNames: [],
                    roleName: this.name,
                    target: roomName,
                };

            Memory.roles.roomRoles.push(state);
        }

        return state;
    },

    onCreepSpawning: function (creepName: string, roleState: RoleStateRoom) {
        roleState.roleCreepNames.push(creepName);
    }
};
